package com.xwder.summary.datatype;

import org.junit.Test;

/**
 * byte 测试
 * 参考：byte基础数据类型理解.md、原码补码和反码.md
 *
 * @author xwder
 * @date 2021/3/17 10:11
 **/
public class ByteTest {

    @Test
    public void byteTest() {
        int b = 456;
        byte test = (byte) b;
        System.out.println(test);

        byte a = 127;
        // += 不会导致类型转换
        a += 1;
        // a = a+1 会导致类型强制转换为int
        // a = a + 1;
        System.out.println(a);
    }

}
