package com.xwder.summary.webp2jpg;

import com.luciad.imageio.webp.WebPReadParam;
import com.luciad.imageio.webp.WebPWriteParam;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * webp图片转换工具类
 * @author xwder
 * @date 2021/4/14 17:50
 **/
public class Webp2JpgUtil {


    /**
     * webp 2 jpg
     *
     * @param webpFilePath webp文件路径
     * @param newFilePath  转换后的文件路径
     * @return
     */
    public static String webp2Jpg(String webpFilePath, String newFilePath) {
        // Obtain a WebP ImageReader instance
        ImageReader reader = ImageIO.getImageReadersByMIMEType("image/webp").next();

        // Configure decoding parameters
        WebPReadParam readParam = new WebPReadParam();
        readParam.setBypassFiltering(true);

        try {
            // Configure the input on the ImageReader
            File file = new File(webpFilePath);
            if (!file.exists()) {
                return null;
            }
            reader.setInput(new FileImageInputStream(file));
            String fileName = file.getName().replace(".webp", "");
            String jpgFilePath = newFilePath + File.separator + fileName + ".jpg";

            // Decode the image
            BufferedImage image = reader.read(0, readParam);

            // png文件比较打
            //ImageIO.write(image, "png", new File(jpgFilePath));
            ImageIO.write(image, "jpg", new File(jpgFilePath));
            //ImageIO.write(image, "jpeg", new File(jpgFilePath));
            System.out.println(webpFilePath + " 转换jpg成功: " + jpgFilePath);
            return jpgFilePath;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


    /**
     * jpg 2 webp
     *
     * @param jpgFilePath webp文件路径
     * @param newFilePath 转换后的文件路径
     * @return
     */
    public static String Jpg2Webp(String jpgFilePath, String newFilePath) {

        File jpgFile = new File(jpgFilePath);
        if (!jpgFile.exists()) {
            return null;
        }
        String fileName = jpgFile.getName().split("\\.")[0];
        String webpFilePath = newFilePath + File.separator + fileName + ".webp";

        ImageReader reader = null;
        WebPReadParam readParam = null;

        try {
            // Obtain an image to encode from somewhere
            BufferedImage image = ImageIO.read(jpgFile);

            // Obtain a WebP ImageWriter instance
            ImageWriter writer = ImageIO.getImageWritersByMIMEType("image/webp").next();

            // Configure encoding parameters
            WebPWriteParam writeParam = new WebPWriteParam(writer.getLocale());
            writeParam.setCompressionMode(WebPWriteParam.MODE_DEFAULT);

            // Configure the output on the ImageWriter
            writer.setOutput(new FileImageOutputStream(new File(webpFilePath)));

            // Encode
            writer.write(null, new IIOImage(image, null, null), writeParam);

            // Obtain a WebP ImageReader instance
            reader = ImageIO.getImageReadersByMIMEType("image/webp").next();

            // Configure decoding parameters
            readParam = new WebPReadParam();
            readParam.setBypassFiltering(true);

            System.out.println(jpgFilePath + " 转换jpg成功: " + webpFilePath);
            return webpFilePath;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        /*File webpDir = new File("E:\\图片\\2cy");
        if (webpDir.isDirectory()) {
            File[] files = webpDir.listFiles();
            assert files != null;
            List<File> fileList = Arrays.asList(files);
            List<String> collect = fileList.parallelStream().map(file -> {
                String webpFilePath = file.getAbsolutePath();
                String newJpgFilePath = file.getParentFile().getAbsolutePath() + File.separator + "jpg";
                File newJpgFile = new File(newJpgFilePath);
                if (!newJpgFile.exists()) {
                    newJpgFile.mkdir();
                }
                return webp2Jpg(webpFilePath, newJpgFilePath);
            }).collect(Collectors.toList());
            System.out.println(collect);
        }*/

        File jpgDir = new File("E:\\图片\\2cy\\jpg");
        if (jpgDir.isDirectory()) {
            File[] files = jpgDir.listFiles();
            assert files != null;
            List<File> fileList = Arrays.asList(files);
            List<String> collect = fileList.parallelStream().map(file -> {
                String jpgFilePath = file.getAbsolutePath();
                String newWebpFilePath = file.getParentFile().getAbsolutePath() + File.separator + "webp";
                File newWebpFile = new File(newWebpFilePath);
                if (!newWebpFile.exists()) {
                    newWebpFile.mkdir();
                }
                return Jpg2Webp(jpgFilePath, newWebpFilePath);
            }).collect(Collectors.toList());
            System.out.println(collect);
        }

    }
}
