package com.xwder.newfeatures8.lambda.employee;

/**
 * @author xwder
 * @date 2019/9/16 22:17
 */
public class FilterEmployeeBySalary implements MyPredicate<Employee> {

    /**
     * 过滤工资
     *
     * @param employee
     * @return
     */
    @Override
    public boolean filterCondition(Employee employee) {
        return employee.getSalary() > 7000.00;
    }
}
