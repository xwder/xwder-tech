package com.xwder.netty.groupchat;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author xwder
 * @date 2021/3/23 10:15
 **/
public class GroupChatClientHandler extends SimpleChannelInboundHandler<String> {

    private static final Logger logger = LoggerFactory.getLogger(SimpleChannelInboundHandler.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        logger.info(msg);
    }
}
