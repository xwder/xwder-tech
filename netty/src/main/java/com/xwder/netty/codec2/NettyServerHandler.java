package com.xwder.netty.codec2;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 1.我们自定义一个Handler 需要继续netty 规定好的某个HandlerAdapter(规范)
 * 2.这时我们自定义一个Handler , 才能称为一个handler
 *
 * @author xwder
 * @date 2021/3/22 09:48
 **/
public class NettyServerHandler extends SimpleChannelInboundHandler<MyDataInfo.MyMessage> {

    private static final Logger logger = LoggerFactory.getLogger(NettyServerHandler.class);

    /**
     * 读取数据实际(这里我们可以读取客户端发送的消息)
     *
     * @param ctx 上下文对象, 含有 管道pipeline , 通道channel, 地址
     * @param msg
     */
    @Override
    public void channelRead0(ChannelHandlerContext ctx, MyDataInfo.MyMessage msg) throws Exception {

        //根据dataType 来显示不同的信息
        MyDataInfo.MyMessage.DataType dataType = msg.getDataType();
        if (dataType == MyDataInfo.MyMessage.DataType.StudentType) {

            MyDataInfo.Student student = msg.getStudent();
            logger.info("客户端发送的数据：id:{},name:{}", student.getId(), student.getName());

        } else if (dataType == MyDataInfo.MyMessage.DataType.WorkerType) {
            MyDataInfo.Worker worker = msg.getWorker();
            logger.info("客户端发送的数据：name:{},age:{}", worker.getName(), worker.getAge());
        } else {
            logger.error("传输的类型不正确");
        }

    }

    /**
     * 读取数据完毕
     *
     * @param ctx 上下文对象, 含有 管道pipeline , 通道channel, 地址
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.writeAndFlush(Unpooled.copiedBuffer("hello, 客户端~(>^ω^<)喵1 ~~~", CharsetUtil.UTF_8));
    }

    /**
     * 处理异常, 一般是需要关闭通道
     *
     * @param ctx   上下文对象, 含有 管道pipeline , 通道channel, 地址
     * @param cause 异常信息
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.close();
    }
}
