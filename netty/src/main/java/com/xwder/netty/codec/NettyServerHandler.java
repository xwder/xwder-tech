package com.xwder.netty.codec;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 1.我们自定义一个Handler 需要继续netty 规定好的某个HandlerAdapter(规范)
 * 2.这时我们自定义一个Handler , 才能称为一个handler
 *
 * @author xwder
 * @date 2021/3/22 09:48
 **/
public class NettyServerHandler extends SimpleChannelInboundHandler<StudentPOJO.Student> {

    private static final Logger logger = LoggerFactory.getLogger(NettyServerHandler.class);

    /**
     * 读取数据实际(这里我们可以读取客户端发送的消息)
     *
     * @param ctx 上下文对象, 含有 管道pipeline , 通道channel, 地址
     * @param msg
     */
    @Override
    public void channelRead0(ChannelHandlerContext ctx, StudentPOJO.Student msg) throws Exception {
        logger.info("客户端发送的数据：id:{},name:{}", msg.getId(), msg.getName());
    }

    /**
     * 读取数据完毕
     *
     * @param ctx 上下文对象, 含有 管道pipeline , 通道channel, 地址
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.writeAndFlush(Unpooled.copiedBuffer("hello, 客户端~(>^ω^<)喵1 ~~~", CharsetUtil.UTF_8));
    }

    /**
     * 处理异常, 一般是需要关闭通道
     *
     * @param ctx   上下文对象, 含有 管道pipeline , 通道channel, 地址
     * @param cause 异常信息
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.close();
    }
}
