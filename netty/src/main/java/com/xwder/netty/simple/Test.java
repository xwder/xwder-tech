package com.xwder.netty.simple;

import io.netty.util.NettyRuntime;

/**
 * 获取系统核心数量
 *
 * @author xwder
 * @date 2021-03-20 15:45:25
 */
public class Test {
    public static void main(String[] args) {
        System.out.println(NettyRuntime.availableProcessors());
    }
}