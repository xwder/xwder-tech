package com.xwder.netty.boundhandler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author xwder
 * @date 2021/3/25 10:10
 **/
public class ServerHandler extends SimpleChannelInboundHandler<Long> {

    private static final Logger logger = LoggerFactory.getLogger(ServerHandler.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Long msg) throws Exception {
        logger.info("从客户端:{}读取到long:{}", ctx.channel().remoteAddress().toString().substring(1), msg);
    }
}
