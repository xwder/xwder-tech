package com.xwder.netty.boundhandler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author xwder
 * @date 2021/3/25 10:33
 **/
public class ClientHandler extends SimpleChannelInboundHandler<Long> {

    private static final Logger logger = LoggerFactory.getLogger(ClientHandler.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Long msg) throws Exception {
        logger.info("服务器IP:{},收到服务器消息:{}", ctx.channel().remoteAddress().toString().substring(1), msg);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("ClientHandler channelActive()发送数据 ~~");
        ctx.writeAndFlush(1234567L);
    }
}
