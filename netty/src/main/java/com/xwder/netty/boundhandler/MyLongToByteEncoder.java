package com.xwder.netty.boundhandler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author xwder
 * @date 2021/3/25 10:07
 **/
public class MyLongToByteEncoder extends MessageToByteEncoder<Long> {

    private static final Logger logger = LoggerFactory.getLogger(MyLongToByteEncoder.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, Long msg, ByteBuf out) throws Exception {
        logger.info("MyLongToByteEncoder encode() 被调用,msg:{}", msg);
    }
}
