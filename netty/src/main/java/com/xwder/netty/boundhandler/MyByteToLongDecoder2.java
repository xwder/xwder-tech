package com.xwder.netty.boundhandler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author xwder
 * @date 2021/3/25 10:38
 **/
public class MyByteToLongDecoder2 extends ReplayingDecoder<Void> {

    private static final Logger logger = LoggerFactory.getLogger(MyByteToLongDecoder2.class);

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        logger.info("MyByteToLongDecoder2 decode() 被调用 ~~");
        //在 ReplayingDecoder 不需要判断数据是否足够读取，内部会进行处理判断
        out.add(in.readLong());
    }
}