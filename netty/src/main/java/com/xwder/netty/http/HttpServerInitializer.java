package com.xwder.netty.http;

import com.xwder.netty.simple.NettyServer;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author xwder
 * @date 2021/3/23 09:24
 **/
public class HttpServerInitializer extends ChannelInitializer<SocketChannel> {

    private static final Logger logger = LoggerFactory.getLogger(NettyServer.class);

    @Override
    protected void initChannel(SocketChannel ch) {

        //得到管道
        ChannelPipeline pipeline = ch.pipeline();

        //加入一个netty 提供的httpServerCodec codec =>[coder - decoder]
        //HttpServerCodec 说明
        //1. HttpServerCodec 是netty 提供的处理http的 编-解码器
        pipeline.addLast("MyHttpServerCodec",new HttpServerCodec());
        //2. 增加一个自定义的 handler
        pipeline.addLast("MyTestHttpServerHandler", new HttpServerHandler());

        logger.info("HttpServerInitializer initChannel ok ~~");
    }
}
