package com.xwder.nettyclient;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author xwder
 * @date 2021/3/31 14:56
 **/
public class NettyClientIdeTrigger2 extends SimpleChannelInboundHandler {

    private final Logger logger = LoggerFactory.getLogger(NettyClientIdeTrigger2.class);


    private NettyClient nettyClient;

    public NettyClientIdeTrigger2(NettyClient nettyClient) {
        this.nettyClient = nettyClient;
    }


    /**
     * 多大连续5次未收到 server 的ping消息
     */
    private final static int MAX_UN_REC_PING_TIMES = 5;

    /**
     * 连续多少次未收到 server 的ping消息
     */
    private AtomicInteger unRecPingTimes = new AtomicInteger();

    /**
     * 客户端发送给服务器端的心跳开始字符串 心跳字符串：XWDER-CLIENT#yyyyMMddHHmmssSSS  XWDER-CLIENT#202103311525000
     */
    private final String CLIENT_PING_STR_BEGIN = "XWDER-CLIENT#";
    /**
     * 服务器端发送给客户端的心跳开始字符串 心跳字符串：XWDER-SERVER#yyyyMMddHHmmssSSS
     */
    private final String SERVER_PONG_STR_BEGIN = "XWDER-SERVER#";

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
        super.channelActive(ctx);
        logger.info("连接服务器成功!" + insocket.getAddress());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        InetSocketAddress inSocket = (InetSocketAddress) ctx.channel().remoteAddress();
        logger.info("关闭与服务器:[{}]关闭", inSocket.getAddress());
        // 定时线程 断线重连
        EventLoop eventLoop = ctx.channel().eventLoop();
        eventLoop.schedule(() -> this.nettyClient.reConnect(), 3, TimeUnit.SECONDS);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
        InetSocketAddress inSocket = (InetSocketAddress) ctx.channel().remoteAddress();
        logger.error("与服务器:[{}]连接异常! ", inSocket.getAddress(), cause);
        Channel channel = ctx.channel();
        if (channel.isActive()) {
            channel.close();
        } else {
            ctx.fireExceptionCaught(cause);
        }
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        InetSocketAddress inSocket = (InetSocketAddress) ctx.channel().remoteAddress();
        logger.info("服务器:[{}] handlerRemoved()", inSocket.getAddress());
        super.handlerRemoved(ctx);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        unRecPingTimes.set(0);
        if (msg instanceof ByteBuf) {
            ByteBuf ByteBuf = (ByteBuf) msg;
            int len = 50;
            if (ByteBuf.readableBytes() < len) {
                len = ByteBuf.readableBytes();
            }
            byte[] pingBytes = new byte[len];
            ByteBuf.getBytes(0, pingBytes, 0, len);
            String pingStr = new String(pingBytes);
            if (StrUtil.startWith(pingStr, SERVER_PONG_STR_BEGIN)) {
                logger.info("收到服务器心跳包:[{}],回应心跳包:[{}]", pingStr.trim());
                return;
            }
            ByteBuf newBuf = Unpooled.copiedBuffer(ByteBuf);
            ByteBuf.release();
            ctx.fireChannelRead(newBuf);
        }
    }


    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        InetSocketAddress inSocket = (InetSocketAddress) ctx.channel().remoteAddress();
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            String eventType = null;
            switch (event.state()) {
                case READER_IDLE:
                    eventType = "读空闲";
                    break;
                case WRITER_IDLE:
                    eventType = "写空闲";
                    break;
                case ALL_IDLE:
                    eventType = "读写空闲";
                    break;
                default:
                    break;
            }
            logger.debug("服务器:[{}] ping超时[{}] ~~", inSocket.getAddress(), eventType);

            if (event.state().equals(IdleState.ALL_IDLE)) {
                // 失败计数器次数大于等于5次的时候，关闭链接，等待client重连
                if (unRecPingTimes.get() >= MAX_UN_REC_PING_TIMES) {
                    // 连续超过N次未收到client的ping消息，那么关闭该通道，等待client重连
                    ctx.channel().close();
                    logger.info("连续未收到服务器:[{}]消息，关闭通道，等待重连!", inSocket.getAddress());
                } else {
                    // 失败计数器加1
                    unRecPingTimes.addAndGet(1);
                    if (ctx.channel().isWritable()) {
                        String pongStr = CLIENT_PING_STR_BEGIN + DateUtil.format(new Date(), DatePattern.PURE_DATETIME_MS_PATTERN);
                        byte[] pongBytes = pongStr.getBytes();
                        ByteBuf pongBuf = Unpooled.buffer(pongBytes.length);
                        pongBuf.writeBytes(pongBytes);
                        ctx.channel().writeAndFlush(pongBuf);
                        logger.info("发送心跳包:[{}]", pongStr);
                    }
                }
            }
        }

        super.userEventTriggered(ctx, evt);
    }
}
