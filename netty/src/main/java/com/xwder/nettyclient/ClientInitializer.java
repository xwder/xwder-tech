package com.xwder.nettyclient;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * @author xwder
 * @date 2021/3/31 14:49
 **/
public class ClientInitializer extends ChannelInitializer<SocketChannel> {

    private NettyClient nettyClient;

    public ClientInitializer(NettyClient nettyClient) {
        this.nettyClient = nettyClient;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        //得到pipeline
        ChannelPipeline pipeline = ch.pipeline();
        //加入相关handler
        pipeline
                .addLast(new ChunkedWriteHandler())
                .addLast(new IdleStateHandler(0, 0, 3, TimeUnit.SECONDS))
                // 字符串解码 和 编码
                .addLast(new NettyClientIdeTrigger(nettyClient))
        ;
    }
}
