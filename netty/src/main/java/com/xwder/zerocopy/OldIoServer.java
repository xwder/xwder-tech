package com.xwder.zerocopy;

import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * old id test server
 *
 * @author xwder
 * @date 2021/3/20 13:10
 */
public class OldIoServer {

    public static void main(String[] args) throws Exception {
        int port = 6666;
        ServerSocket serverSocket = new ServerSocket(port);

        while (true) {
            Socket socket = serverSocket.accept();

            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());

            byte[] bytes = new byte[4096];
            while (true) {
                int readCount = dataInputStream.read(bytes, 0, bytes.length);
                if (-1 == readCount) {
                    break;
                }
            }
        }

    }
}
