package com.xwder.zerocopy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.net.InetSocketAddress;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;

/**
 * new io test client
 *
 * @author xwder
 * @date 2021/3/20 13:23
 */
public class NewIoClient {

    public static final Logger logger = LoggerFactory.getLogger(NewIoClient.class);

    public static void main(String[] args) throws Exception {

        InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost", 6666);
        SocketChannel socketChannel = SocketChannel.open(inetSocketAddress);

        String fileName = "/Users/xwder/xwderWorkspace/xwder-tech/netty/protoc-3.6.1-win32.zip";
        // 获取文件channel
        FileChannel fileChannel = new FileInputStream(fileName).getChannel();

        // 准备发送数据
        long startTime = System.currentTimeMillis();

        long transferCounter = fileChannel.transferTo(0, fileChannel.size(), socketChannel);

        logger.info("发送字节数:{},耗时:{}", transferCounter, (System.currentTimeMillis() - startTime));

        fileChannel.close();

    }
}
