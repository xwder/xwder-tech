package com.xwder.zerocopy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.Socket;

/**
 * old io test client
 * @author xwder
 * @date 2021/3/20 13:15
 */
public class OldIoClient {

    public static final Logger logger = LoggerFactory.getLogger(OldIoClient.class);

    public static void main(String[] args) throws Exception {
        Socket socket = new Socket("localhost", 6666);

        String fileName = "/Users/xwder/xwderWorkspace/xwder-tech/netty/protoc-3.6.1-win32.zip";
        InputStream inputStream = new FileInputStream(fileName);

        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

        byte[] buffer = new byte[4096];
        long readCount;
        long total = 0;

        long startTime = System.currentTimeMillis();

        while ((readCount = inputStream.read(buffer)) >= 0) {
            total += readCount;
            dataOutputStream.write(buffer);
        }

        logger.info("发送字节数:{},耗时:{}", total, (System.currentTimeMillis() - startTime));
        dataOutputStream.close();
        socket.close();
        inputStream.close();
    }
}
