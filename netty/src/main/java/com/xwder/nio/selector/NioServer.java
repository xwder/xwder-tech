package com.xwder.nio.selector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * Selector SelectionKey demo server端
 *
 * @author xwder
 * @date 2021/3/17 21:09
 */
public class NioServer {

    private static final Logger logger = LoggerFactory.getLogger(NioServer.class);

    public static void main(String[] args) throws Exception {

        // 创建ServerSocketChannel -> ServerSocket
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        // 获取一个Selector
        Selector selector = Selector.open();
        // 绑定端口在服务器端检讨
        serverSocketChannel.socket().bind(new InetSocketAddress(6666));
        // 设置为非阻塞模式
        serverSocketChannel.configureBlocking(false);

        // 把serverSocketChannel注册到Selector上，关注事件 OP_ACCEPT
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        logger.info("注册后的SelectorKey数量：{}", selector.keys().size());

        // 循环等待客户端连接
        while (true) {
            // 等待1秒，如果没有事件发生则返回
            if (selector.select(1000) == 0) {
                logger.info("服务器等待1秒，未发现客户端连接");
                continue;
            }
            // 如果返回的结果>0,表示已经获取到客户端的事件 通过selectionKey集合可以获取到关注到的事件
            // selector.selectedKeys() 返回关注到的事件集合
            // selectionKey 反向获取关注事件的通道
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            logger.info("服务器selector获取到关注的事件数量：{}", selectionKeys.size());

            // 遍历获取到的事件
            Iterator<SelectionKey> keyIterator = selectionKeys.iterator();
            if (keyIterator.hasNext()) {
                // 获取到SelectionKey
                SelectionKey selectionKey = keyIterator.next();
                // 根据selectionKey事件不通的种类分别处理
                // OP_ACCEPT 客户端新的连接事件
                if (selectionKey.isAcceptable()) {
                    // 为该客户端连接建立一个socketChannel
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    logger.info("服务器端为客户端请求建立SocketChannel:{}", socketChannel.hashCode());
                    // 设置 socketChannel为非阻塞
                    socketChannel.configureBlocking(false);

                    // 将给socketChannel注册到Selector并关注事件 OP_READ，并同时关联一个buffer
                    socketChannel.register(selector, SelectionKey.OP_READ, ByteBuffer.allocate(1024));
                    logger.info("服务器端客户端连接后，注册的selectionKey的数量:{}", selector.selectedKeys().size());
                }

                // OP_READ 客户端发送数据事件
                if (selectionKey.isReadable()) {
                    // 通过selectorKey 反向获取channel
                    SocketChannel channel = (SocketChannel) selectionKey.channel();
                    // 获取该channel关联的buffer
                    ByteBuffer byteBuffer = (ByteBuffer) selectionKey.attachment();
                    channel.read(byteBuffer);
                    logger.info("服务器端收到客户端发送数据:{}", new String(byteBuffer.array()));
                }

                // 手动从集合中移除当前selectionKey防止重复操作
                keyIterator.remove();
            }
        }
    }
}
