# springboot example

包含的功能：

- 统一异常处理
- Ehcache缓存
- jsr参数校验
- 配置文件参数读取 @ConfigurationProperties,@Conditional
- jdbcTemplate 数据库增删查改
- xss参数过滤