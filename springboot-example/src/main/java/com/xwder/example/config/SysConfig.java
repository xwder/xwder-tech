package com.xwder.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * 系统的配置文件
 *
 * @author xwder
 * @date 2021/3/31 10:10
 **/
@Component
public class SysConfig {

    @Autowired
    private Environment environment;

    public String getStr(String key, String defaultValue) {
        return environment.getProperty(key, defaultValue);
    }
}
