/*
 Navicat Premium Data Transfer

 Source Server         : localhost_mysql
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : localhost:3306
 Source Schema         : xwder

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 17/03/2021 09:56:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id 主键',
  `open_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'qq用户表openid',
  `github_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'github用户名',
  `user_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `is_manager` tinyint(3) NULL DEFAULT 0 COMMENT '是否是管理员 1-是 0-否',
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
  `email` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `sex` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '性别 0-女 1-男 2-未知',
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `register_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后一次修改个人信息时间',
  `status` tinyint(3) UNSIGNED NULL DEFAULT NULL COMMENT '账号状态 0已注册未验证邮箱，1验证邮箱，2验证了手机，3验证了手机加邮箱',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  `salt` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '加密的盐',
  `login_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上次登录的IP',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录的时间',
  `is_available` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '0-删除不可用，1-未删除可用',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `gmt_create` datetime(0) NULL DEFAULT NULL,
  `gmt_modified` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '76C91088EF7B42DFDAD53AEDE03B354E', 'xwder', 'xwder', NULL, 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201010094334764.png', 'zFNuhIL6ctS2nizL', NULL, '2021-03-10 19:21:52', 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (2, NULL, NULL, 'admin', NULL, 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201005231659443.gif', 'zFNuhIL6ctS2nizL', NULL, NULL, 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (3, NULL, NULL, 'Joe', NULL, 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201005231659443.gif', 'zFNuhIL6ctS2nizL', NULL, NULL, 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (27, NULL, NULL, 'wande', NULL, '新会员', 0, NULL, NULL, 2, '中国', '2020-10-24 14:34:57', '2020-10-24 14:34:57', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201005231659443.gif', 'V1NKjxaJQoBeEBFI', NULL, NULL, 1, NULL, '2020-10-24 14:34:57', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (30, '55D8714147CD38355A3F34FBFF495558', NULL, NULL, NULL, 'Joe', 0, NULL, NULL, 2, NULL, '2020-11-08 01:58:31', '2020-11-08 01:58:31', 3, 'http://qzapp.qlogo.cn/qzapp/101787029/55D8714147CD38355A3F34FBFF495558/30', NULL, NULL, '2021-01-26 22:40:32', 1, NULL, '2020-11-08 01:58:31', '2021-01-26 22:40:33');
INSERT INTO `user` VALUES (31, 'D709DC10B15AF75F96B1848145845D23', NULL, NULL, NULL, '平静', 0, NULL, NULL, 2, NULL, '2020-11-28 10:24:02', '2020-11-28 10:24:02', 3, 'http://qzapp.qlogo.cn/qzapp/101787029/D709DC10B15AF75F96B1848145845D23/30', NULL, NULL, '2020-11-28 10:24:02', 1, NULL, '2020-11-28 10:24:02', '2020-11-28 10:24:02');
INSERT INTO `user` VALUES (32, NULL, NULL, '000001', NULL, '新会员', 0, NULL, NULL, 2, '中国', '2021-02-11 02:41:26', '2021-02-11 02:41:26', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201005231659443.gif', 'USVdY5M1AyaAPCnG', NULL, NULL, 1, NULL, '2021-02-11 02:41:26', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (33, 'B6037B621C8AB5159A976FE9EA8A3A12', NULL, NULL, NULL, '怪我喽？', 0, NULL, NULL, 2, NULL, '2021-02-14 23:19:02', '2021-02-14 23:19:02', 3, 'http://qzapp.qlogo.cn/qzapp/101787029/B6037B621C8AB5159A976FE9EA8A3A12/30', NULL, NULL, '2021-02-14 23:19:02', 1, NULL, '2021-02-14 23:19:02', '2021-02-14 23:19:02');
INSERT INTO `user` VALUES (34, '308E7FE41E26112EE6846CB7B526F02C', NULL, NULL, NULL, '୧⍤⃝ღ', 0, NULL, NULL, 2, NULL, '2021-02-23 11:14:02', '2021-02-23 11:14:02', 3, 'http://qzapp.qlogo.cn/qzapp/101787029/308E7FE41E26112EE6846CB7B526F02C/30', NULL, NULL, '2021-02-23 11:14:02', 1, NULL, '2021-02-23 11:14:02', '2021-02-23 11:14:02');
INSERT INTO `user` VALUES (35, NULL, NULL, 'wangyd', NULL, '新会员', 0, NULL, NULL, 2, '中国', '2021-02-28 21:54:24', '2021-02-28 21:54:24', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201005231659443.gif', 'unfc5lXKTRliRXN6', NULL, NULL, 1, NULL, '2021-02-28 21:54:24', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (36, '597BA46D47D31DF550C466CDB044B3B9', NULL, NULL, NULL, '山海', 0, NULL, NULL, 2, NULL, '2021-03-03 23:29:45', '2021-03-03 23:29:45', 3, 'http://qzapp.qlogo.cn/qzapp/101787029/597BA46D47D31DF550C466CDB044B3B9/30', NULL, NULL, '2021-03-03 23:29:45', 1, NULL, '2021-03-03 23:29:45', '2021-03-03 23:29:45');
INSERT INTO `user` VALUES (45, '76C91088EF7B42DFDAD53AEDE03B354E', 'xwder', 'xwder', NULL, 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201010094334764.png', 'zFNuhIL6ctS2nizL', NULL, '2021-03-10 19:21:52', 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (46, '76C91088EF7B42DFDAD53AEDE03B354E', 'xwder', '用户名', '密码', 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201010094334764.png', 'zFNuhIL6ctS2nizL', NULL, '2021-03-10 19:21:52', 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (47, '76C91088EF7B42DFDAD53AEDE03B354E', 'xwder', '用户名1', '密码1', 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201010094334764.png', 'zFNuhIL6ctS2nizL', NULL, '2021-03-10 19:21:52', 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (48, '76C91088EF7B42DFDAD53AEDE03B354E', 'xwder', '用户名2', '密码2', 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201010094334764.png', 'zFNuhIL6ctS2nizL', NULL, '2021-03-10 19:21:52', 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (49, '76C91088EF7B42DFDAD53AEDE03B354E', 'xwder', '用户名3', '密码3', 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201010094334764.png', 'zFNuhIL6ctS2nizL', NULL, '2021-03-10 19:21:52', 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (50, '76C91088EF7B42DFDAD53AEDE03B354E', 'xwder', '用户名4', '密码', 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201010094334764.png', 'zFNuhIL6ctS2nizL', NULL, '2021-03-10 19:21:52', 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (51, '76C91088EF7B42DFDAD53AEDE03B354E', 'xwder', '用户名5', '密码', 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201010094334764.png', 'zFNuhIL6ctS2nizL', NULL, '2021-03-10 19:21:52', 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (52, '76C91088EF7B42DFDAD53AEDE03B354E', 'xwder', '用户名6', '密码', 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201010094334764.png', 'zFNuhIL6ctS2nizL', NULL, '2021-03-10 19:21:52', 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (53, '76C91088EF7B42DFDAD53AEDE03B354E', 'xwder', '用户名7', '密码', 'OneDay', 1, NULL, NULL, NULL, NULL, '2020-07-24 15:39:43', '2020-07-24 15:39:43', 0, 'https://cdn.xwder.com/image/blog/xwder/1-20201010094334764.png', 'zFNuhIL6ctS2nizL', NULL, '2021-03-10 19:21:52', 1, NULL, '2020-07-24 15:39:43', '2021-03-17 09:56:25');
INSERT INTO `user` VALUES (54, NULL, NULL, 'Joe', '密码', NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-03-12 14:21:43');

SET FOREIGN_KEY_CHECKS = 1;
